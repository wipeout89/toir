<?php

namespace App\Console\Commands;

use App\Entities\Schedule;
use App\Entities\Task;
use App\Entities\TaskView;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $need_date = CarbonImmutable::now()->add(TaskView::DAYS_BEFORE_TASK, 'day');

        $tasks_view = TaskView::with('task')
            ->where('status', Task::NOT_RELEVANT)
            ->orWhere('status', Task::COMPLETED)
            ->get();

        foreach ($tasks_view as $task_view){
            $task_view->task->update(['status' => $task_view->status]);
            $task_view->delete();
        }

        $tasks_view = TaskView::orderBy('rank')->get();

        $i = 1;

        foreach ($tasks_view as $task_view){
            $task_view->update([
                'rank' => $i++
            ]);
        }

        $tasks_view = $tasks_view->pluck('task_id')->toArray();

        $tasks = Task::where('status', Task::RELEVANT)
            ->where('run_time', '<', $need_date->format('Y-m-d'))
            ->whereNotIn('id', $tasks_view)
            ->get();

        foreach ($tasks as $task){
            try{
                TaskView::create(
                    [
                        'title' => $task->title,
                        'spares' => $task->spares,
                        'period' => $task->period,
                        'units' => $task->units,
                        'equipment_id' => $task->equipment_id,
                        'with_stop' => $task->with_stop,
                        'qualification' => $task->qualification,
                        'status' => $task->status,
                        'run_time' => $task->run_time,
                        'rank' => $i++,
                        'task_id' => $task->id
                    ]
                );
            } catch (\Exception $exception){
                print $exception->getMessage();
            }
        }
    }
}
