<?php

namespace App\Console;

use App\Entities\Schedule as MySchedule;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function (){
            $my_schedules = MySchedule::where('is_complete', 0)->where('run_time', '<', date('Y-m-d H:i:s'))->get();
            foreach ($my_schedules as $my_schedule){
//                $schedule_id = null;
//                $schedule_id = DB::table('schedule_log')->insertGetId([
//                    'schedule_id' => $my_schedule->id,
//                    'start_time' => date('Y-m-d H:i:s')
//                ]);
                $className = $my_schedule->task_type;
                $class = new $className($my_schedule->entity_id, $my_schedule->params);
                try {
                    if ($class->run()) {
                        $my_schedule->is_complete = true;
                        $my_schedule->save();
                        $result = 'true';
                    } else {
                        $result = 'false';
                    }
                } catch (\Exception $e){
                    $result = 'exception';
                    report($e);
                } /*finally {
                    DB::table('schedule_log')
                        ->where('id', $schedule_id)
                        ->update([
                            'end_time' => date('Y-m-d H:i:s'),
                            'result' => $result
                        ]);
                }*/
            }
        })->name('schedule')->withoutOverlapping(30)->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
