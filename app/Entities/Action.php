<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [
        'name',
        'route'
    ];

    public function groups(){
        return $this->belongsToMany(Group::class);
    }
}
