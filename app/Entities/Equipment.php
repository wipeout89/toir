<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    const RELEVANT = 'RELEVANT';
    const NOT_RELEVANT = 'NOT_RELEVANT';

    public static $statuses = [
        self::RELEVANT => self::RELEVANT,
        self::NOT_RELEVANT => self::NOT_RELEVANT,
    ];

    protected $fillable = [
        'name',
        'parent_id',
        'status'
    ];

    protected $attributes = [
        'status' => self::RELEVANT
    ];

    public function save(array $options = []){
        $this->full_name = ' / '.$this->name;
        $full_name = [];
        $parent = Equipment::where('id', $this->parent_id)->first();
        while ($parent){
            $full_name[] = $parent->name;
            $parent = Equipment::where('id', $parent->parent_id)->first();
        }
        for($i = 0; $i < count($full_name); $i++){
            $this->full_name = ' / '.$full_name[$i].$this->full_name;
        }

        $this->full_name = mb_substr($this->full_name, 3);
        parent::save();
    }
}
