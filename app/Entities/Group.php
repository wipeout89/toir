<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    const ACTIVE = 'ACTIVE';
    const INACTIVE = 'INACTIVE';

    const ADMIN_GROUP_ID = 1;

    public static $statuses = [
        self::ACTIVE => 'Используется',
        self::INACTIVE => 'Не используется'
    ];

    protected $fillable = [
        'name',
        'status'
    ];

    protected $attributes = [
        'status' => self::ACTIVE
    ];

    public function users(){
        return $this->hasMany(User::class);
    }

    public function actions(){
        return $this->belongsToMany(Action::class);
    }
}
