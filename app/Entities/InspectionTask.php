<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class InspectionTask extends Model
{
    const RELEVANT = 'RELEVANT';
    const NOT_RELEVANT = 'NOT_RELEVANT';
    const COMPLETED = 'COMPLETED';

    const HOUR = 'HOUR';
    const DAY = 'DAY';
    const MONTH = 'MONTH';
    const YEAR = 'YEAR';

    public static $statuses = [
        self::RELEVANT => self::RELEVANT,
        self::NOT_RELEVANT => self::NOT_RELEVANT,
        self::COMPLETED => self::COMPLETED
    ];

    public static $units = [
        self::HOUR => 'В часах',
        self::DAY => 'В днях',
        self::MONTH => 'В месяцах',
        self::YEAR => 'В годах'
    ];

    protected $fillable = [
        'title',
        'period',
        'units',
        'equipment_id',
        'status',
        'run_time'
    ];

    protected $attributes = [
        'status' => self::RELEVANT
    ];

    public function equipment(){
        return $this->belongsTo(Equipment::class);
    }
}
