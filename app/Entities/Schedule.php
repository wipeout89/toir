<?php

namespace App\Entities;

use App\ScheduleTask\SynchronizationTaskAndTaskView;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    const SYNCHRONIZATION_TASK_AND_TASK_VIEW = SynchronizationTaskAndTaskView::class;

    public static $task_types = [
        self::SYNCHRONIZATION_TASK_AND_TASK_VIEW,
    ];

    protected $fillable = [
        'entity_id',
        'params',
        'is_complete',
        'task_type',
        'run_time'
    ];

    protected $attributes = [
        'entity_id' => null,
        'params' => null,
        'is_complete' => false
    ];
}
