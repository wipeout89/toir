<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Task extends Model
{
    const RELEVANT = 'RELEVANT';
    const NOT_RELEVANT = 'NOT_RELEVANT';
    const COMPLETED = 'COMPLETED';

    const HOUR = 'HOUR';
    const DAY = 'DAY';
    const MONTH = 'MONTH';
    const YEAR = 'YEAR';

    const DAYS_BEFORE_TASK = 5;

    public static $statuses = [
        self::RELEVANT => self::RELEVANT,
        self::NOT_RELEVANT => self::NOT_RELEVANT,
        self::COMPLETED => self::COMPLETED
    ];

    public static $units = [
        self::HOUR => 'В часах',
        self::DAY => 'В днях',
        self::MONTH => 'В месяцах',
        self::YEAR => 'В годах'
    ];

    protected $fillable = [
        'title',
        'spares',
        'period',
        'units',
        'group_id',
        'with_stop',
        'equipment_id',
        'run_time',
        'status'
    ];

    protected $attributes = [
        'status' => self::RELEVANT,
        'units' => self::HOUR
    ];

    public function getRunTimeAttribute($value){
        $date = new Carbon($value);
        return $date->format('d.m.Y');
    }

    public function equipment(){
        return $this->belongsTo(Equipment::class);
    }

    public function qualification(){
        return $this->belongsTo(Group::class, 'group_id');
    }
}
