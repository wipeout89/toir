<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TaskView extends Model
{
    const DAYS_BEFORE_TASK = 5;

    protected $fillable = [
        'title',
        'spares',
        'period',
        'units',
        'qualification',
        'with_stop',
        'equipment_id',
        'run_time',
        'status',
        'rank',
        'task_id'
    ];

    public function task(){
        return $this->belongsTo(Task::class);
    }

    public function equipment(){
        return $this->belongsTo(Equipment::class);
    }

    public function getRunTimeAttribute($value){
        $run_time = Carbon::createFromFormat('Y-m-d', $value);
        return $run_time->format('d.m.Y');
    }
}
