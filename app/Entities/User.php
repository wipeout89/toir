<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ACTIVE = 'ACTIVE';
    const INACTIVE = 'INACTIVE';

    public static $statuses = [
        self::ACTIVE => self::ACTIVE,
        self::INACTIVE => self::INACTIVE
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'middle_name',
        'phone',
        'password',
        'status',
        'group_id',
    ];

    protected $attributes = [
        'status' => self::ACTIVE,
        'group_id' => 1
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function group(){
        return $this->belongsTo(Group::class);
    }
}
