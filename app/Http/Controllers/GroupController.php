<?php

namespace App\Http\Controllers;

use App\Entities\Action;
use App\Entities\Group;
use App\Entities\User;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
    }


    public function add()
    {
        $actions = Action::select('name')->distinct()->get();
        $users = User::where('group_id', '1')->get();

        return view('groups.index', [
            'actions' => $actions,
            'users' => $users
        ]);
    }

    public function save(Request $request)
    {
        $group = Group::create([
            'name' => $request->group_name
        ]);
//        dd($request->users);
        $users = User::whereIn('id', $request->ms_users)->get();
//        dd($users);
        foreach ($users as $user) {
            $user->update([
                'group_id' => $group->id
            ]);
        }

        $actions = Action::whereIn('name', $request->ms_actions)->pluck('id')->toArray();

        $group->actions()->sync($actions);

        return redirect(route('group.show-all'))->with([
            'type' => 'success',
            'message' => 'Группа успешно создана'
        ]);

    }

    public function edit(Request $request, $id)
    {
        if($id == Group::ADMIN_GROUP_ID){
            return redirect(route('group.show-all'))->with([
                'type' => 'alert',
                'message' => 'Группа заблокирована для редактирования'
            ]);
        }

        if (!$group = Group::where('id', $id)->first())
            abort(404);

//        $actions = Action::select('name', 'action_group.group_id')
//            ->join('action_group', 'action_group.action_id', '=', 'actions.id')
//            ->distinct()
//            ->get();
        $actions = Action::select('name')->distinct()->get();
        $actions_group = Action::select('name')
            ->join('action_group', 'action_group.action_id', '=', 'actions.id')
            ->where('action_group.group_id', '=', $id)
            ->distinct()
            ->pluck('name')->toArray();

//        dd($actions->toArray());

        $users = User::get();

        return view('groups.index', [
            'group' => $group,
            'group_id' => $id,
            'actions' => $actions,
            'actions_group' => $actions_group,
            'users' => $users
        ]);
    }

    public function update(Request $request)
    {
        if (!empty($request->group_name)) {
            if (!$group = Group::where('id', $request->group_id)->first()) {
                abort(404);
            }
            $group->update([
                'name' => $request->group_name,
            ]);
        }

        $users = User::where('group_id', $request->group_id)->get();
        foreach ($users as $user) {
            $user->update([
                'group_id' => '1'
            ]);
        }

        if (isset($request->ms_users)) {
            $users = User::whereIn('id', $request->ms_users)->get();
            foreach ($users as $user) {
                $user->update([
                    'group_id' => $request->group_id
                ]);
            }
        }

        $actions = [];
        if(isset($request->ms_actions)){
            $actions = Action::whereIn('name', $request->ms_actions)->pluck('id')->toArray();
        }

        $group->actions()->sync($actions);

        return redirect(route('group.show-all'))->with([
            'type' => 'success',
            'message' => 'Группа обновлена'
        ]);
    }


    public function showAll()
    {
        $groups = Group::select('id', 'name', 'status')->distinct()->get();

        $users = User::select('name', 'surname', 'middle_name', 'group_id')->distinct()->get();

        $actions_groups = Action::select('actions.name as action_name', 'groups.id as group_id', 'groups.name as group_name')
            ->join('action_group', 'action_group.action_id', '=', 'actions.id')
            ->leftjoin('groups', 'groups.id', '=', 'action_group.group_id')
            ->distinct()->get();
        return view('groups.show_all', [
            'groups' => $groups,
            'users' => $users,
            'actions_groups' => $actions_groups
        ]);
    }

}






























