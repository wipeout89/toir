<?php

namespace App\Http\Controllers;

use App\Entities\Equipment;
use App\Entities\InspectionTask;
use App\Entities\Task;
use App\Entities\TaskView;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;

class InspectionTaskController extends Controller
{
    public function __construct()
    {
    }

    public function add(){
        $equipments = $this->getEquipmentForForm();

        return view('inspection.add', [
            'equipments' => $equipments
        ]);
    }

    public function save(Request $request){
        $run_time = CarbonImmutable::now()->add($request->period, $request->units);
        $request->request->add(['run_time' => $run_time->format('Y-m-d')]);

        InspectionTask::create($request->except('_token'));

        return redirect(route('inspection.index'))->with([
            'type' => 'success',
            'message' => 'Задача обхода успешно сохранена'
        ]);
    }

    public function getEquipment(Request $request){
        return json_encode($this->getEquipmentForForm($request->id));
    }

    private function getEquipmentForForm($parent_id = null){
        return Equipment::select('id', 'name')->where('parent_id', $parent_id)->get();
    }

    public function checkList(){
        $now_date = CarbonImmutable::now();
        $tasks = InspectionTask::with('equipment:id,full_name')
            ->where('status', InspectionTask::RELEVANT)
            ->where(function ($query) use ($now_date){
                $query->where('run_time', $now_date->format('Y-m-d'))
                    ->orWhere('run_time', '<', $now_date->format('Y-m-d'));
            })->get();

        return view('inspection.check-list', [
            'tasks' => $tasks
        ]);
    }

    public function createTicket(Request $request){
        $inspection_task = InspectionTask::find($request->task_id);

        if($inspection_task){
            $task = Task::create([
                'title' => 'Тикет по задаче: '.$inspection_task->title,
                'description' => $request->commentary,
                'spares' => $request->spares,
                'equipment_id' => $inspection_task->equipment_id,
                'with_stop' => $request->with_stop,
                'group_id' => $request->group_id,
                'run_time' => CarbonImmutable::now()->format('Y-m-d')
            ]);

            return json_encode([
                'result' => true,
                'id' => $request->task_id
            ]);
        }

        return json_encode([
            'result' => false
        ]);
    }

    public function swapStatus(Request $request){
        $inspectionTask = InspectionTask::find($request->id);

        if($inspectionTask->status == InspectionTask::RELEVANT){
            $this->closeTask($inspectionTask);
        } elseif ($inspectionTask->status == InspectionTask::COMPLETED){
            $this->reOpenTask($inspectionTask);
        }
    }

    private function closeTask(&$task){
        $task->status = InspectionTask::COMPLETED;

        $run_time = Carbon::createFromFormat('Y-m-d', $task->run_time);
        $run_time = $run_time->add($task->period, $task->units);

        $new_task = InspectionTask::create([
            'title' => $task->title,
            'period' => $task->period,
            'units' => $task->units,
            'equipment_id' => $task->equipment_id,
            'status' => InspectionTask::RELEVANT,
            'run_time' => $run_time
        ]);

        $task->child_id = $new_task->id;
        $task->save();
    }

    private function reOpenTask(&$task){
        InspectionTask::where('id', $task->child_id)->delete();

        $task->status = InspectionTask::RELEVANT;
        $task->child_id = null;
        $task->save();
    }

    public function saveTicket(Request $request){
        $task = InspectionTask::create([
            'title' => $request->title,
            'equipment_id' => $request->equipment_id,
            'run_time' => date('Y-m-d')
        ]);

        return redirect(route('inspection.show-all'))->with([
            'type' => 'success',
            'message' => 'Успешно создан тикет: '.$task->title,
        ]);
    }
}
