<?php

namespace App\Http\Controllers;

use App\Entities\Equipment;
use App\Entities\Task;
use App\Entities\TaskView;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
    }

    public function add(){
        $equipments = $this->getEquipmentForForm();

        return view('tasks.add', [
            'equipments' => $equipments
        ]);
    }

    public function save(Request $request){
        if(!isset($request->run_time)){
            $run_time = CarbonImmutable::now()->add($request->period, $request->units);
            $request->request->add(['run_time' => $run_time->format('Y-m-d')]);
        } else {
            $run_time = Carbon::createFromFormat('d/m/Y', $request->run_time);
            $request->request->add(['run_time' => $run_time->format('Y-m-d')]);
        }

        Task::create($request->except('_token'));

        return redirect('/')->with([
            'type' => 'success',
            'message' => 'Задача успешно сохранена'
        ]);
    }

    public function showAll(Request $request){
        $days_before_task = (isset($request->days)) ? $request->days : Task::DAYS_BEFORE_TASK;
        $result_date = Carbon::now();
        $result_date = $result_date->add('day', $days_before_task);
        $tasks = Task::with('equipment:id,full_name', 'qualification:id,name')
            ->where('run_time', '<=', $result_date->format('Y-m-d'))
            ->where('status', Task::RELEVANT)
            ->get();

        return view('tasks.show_all', [
            'tasks' => $tasks,
            'days_before_task' => $days_before_task,
        ]);
    }

    public function getEquipment(Request $request){
        $equipment = Equipment::select('full_name')->find($request->id);
        return json_encode([
            'full_name' => $equipment->full_name,
            'equipment' => $this->getEquipmentForForm($request->id)
        ]);
    }

    private function getEquipmentForForm($parent_id = null){
        return Equipment::select('id', 'name')->where('parent_id', $parent_id)->get();
    }

    public function changeRank(Request $request){
        $task_view1 = TaskView::where('rank', $request->oldIndex)->first();
        $task_view2 = TaskView::where('rank', $request->newIndex)->first();

        $task_view1->update([
            'rank' => $request->newIndex
        ]);

        $task_view2->update([
            'rank' => $request->oldIndex
        ]);
    }

    public function getTask(Request $request){
        $task = Task::with('equipment:id,full_name', 'qualification:id,name')->find($request->id);

        return json_encode($task->toArray());
    }

    public function closeTask(Request $request){
        $task = Task::find($request->task_id);
        $task->update([
            'status' => Task::COMPLETED
        ]);

        $new_task_compete = '';

        if((!empty($task->period)) && ($task->units != null)) {
            $run_time = Carbon::createFromFormat('d.m.Y', $task->run_time);
            $run_time = $run_time->add($task->period, $task->units);

            Task::create([
                'title' => $task->title,
                'spares' => $task->spares,
                'period' => $task->period,
                'units' => $task->units,
                'equipment_id' => $task->equipment_id,
                'with_stop' => $task->with_stop,
                'qualification' => $task->qualification,
                'run_time' => $run_time->format('Y-m-d')
            ]);

            $new_task_compete = ' Новая задача успешно поставлена';
        }

        return redirect()->back()->with([
            'type' => 'success',
            'message' => 'Задача закрыта.'.$new_task_compete,
        ]);
    }

    public function saveTicket(Request $request){
        $task = Task::create([
            'title' => 'Тикет',
            'description' => $request->description,
            'spares' => $request->spares,
            'equipment_id' => $request->equipment_id,
            'with_stop' => isset($request->with_stop) ? true : false,
            'group_id' => $request->group_id,
            'run_time' => date('Y-m-d')
        ]);

        $task->update([
            'title' => $task->title.' #'.$task->id
        ]);

        return redirect(route('task.show-all'))->with([
            'type' => 'success',
            'message' => 'Успешно создан '.$task->title,
        ]);
    }
}
