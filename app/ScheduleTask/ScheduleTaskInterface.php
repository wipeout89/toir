<?php


namespace App\ScheduleTask;


interface ScheduleTaskInterface
{
    public function __construct($entity_id, $params);

    public function run();
}