<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Task::class, function (Faker $faker) {
    return [
        'title' => $faker->jobTitle,
        'description' => $faker->text,
        'period' => 1
    ];
});
