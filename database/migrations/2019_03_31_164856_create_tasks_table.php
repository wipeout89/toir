<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('spares')->nullable();
            $table->integer('period')->nullable();
            $table->enum('units', array_keys(\App\Entities\Task::$units))->nullable();
            $table->integer('equipment_id');
            $table->boolean('with_stop')->nullable();
            $table->integer('group_id')->index();
            $table->enum('status', array_keys(\App\Entities\Task::$statuses));
            $table->date('run_time')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
