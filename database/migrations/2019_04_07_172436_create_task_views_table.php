<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_views', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('spares')->nullable();
            $table->integer('period');
            $table->enum('units', array_keys(\App\Entities\Task::$units));
            $table->integer('equipment_id');
            $table->boolean('with_stop')->nullable();
            $table->string('qualification');
            $table->enum('status', array_keys(\App\Entities\Task::$statuses));
            $table->date('run_time');
            $table->integer('rank');
            $table->integer('task_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_views');
    }
}
