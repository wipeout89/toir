<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectionTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title');
            $table->integer('period')->nullable();
            $table->enum('units', array_keys(\App\Entities\InspectionTask::$units))->nullable();
            $table->integer('equipment_id');
            $table->enum('status', array_keys(\App\Entities\InspectionTask::$statuses));
            $table->date('run_time')->index();
            $table->integer('child_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspection_tasks');
    }
}
