<?php

use App\Entities\Action;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actions')->truncate();
        //Добавление пользователей
        Action::insert([
            [
                'route' => "App\Http\Controllers\Auth\RegisterController@showRegistrationForm",
                'name' => 'Добавление пользователя',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'route' => "App\Http\Controllers\Auth\RegisterController@register",
                'name' => 'Добавление пользователя',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'route' => "App\Http\Controllers\GroupController@add",
                'name' => 'Добавление группы',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'route' => "App\Http\Controllers\GroupController@save",
                'name' => 'Добавление группы',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'route' => "App\Http\Controllers\TaskController@add",
                'name' => 'Добавление задач',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'route' => "App\Http\Controllers\TaskController@save",
                'name' => 'Добавление задач',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'route' => "App\Http\Controllers\TaskController@showAll",
                'name' => 'Просмотр задач',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);

    }
}
