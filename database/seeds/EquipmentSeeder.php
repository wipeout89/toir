<?php

use App\Entities\Equipment;
use Illuminate\Database\Seeder;

class EquipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extrusion = Equipment::create([
            'name' => 'Экструзия',
            'parent_id' => null
        ]);

        $array_125 = [
            'Загрузчики',
            'Гравиметрия',
            'Экструдер',
            'Экструзионная голова',
            'Вакуумная ванна',
            'Ванна орошения',
            'Маркиратор и энкодер',
            'Тянущее устройство',
            'Отрезное устройство',
            'Откидной стол',
        ];

        $array_63 = [
            'Загрузчики',
		    'Гравиметрия',
		    'Экструдер',
		    'Экструзионная голова',
		    'Вакуумная ванна',
		    'Ванна орошения',
		    'Маркиратор и энкодер',
		    'Тянущее устройство',
		    'Отрезное устройство',
		    'Откидной стол',
		    'Намотчик',
        ];

        $array_amut = [
            'Загрузчики',
		    'Гравиметрия',
		    'Экструдер',
		    'Экструзионная голова',
		    'Вакуумная ванна',
		    'Ванна орошения',
		    'Маркиратор и энкодер',
		    'Тянущее устройство',
		    'Отрезное устройство',
		    'Откидной стол',
		    'Намотчик',
        ];

        $extrusion_technomatic_125 = Equipment::create([
            'name' => 'Техноматик 125',
            'parent_id' => $extrusion->id,
        ]);

        foreach ($array_125 as $item){
            Equipment::create([
                'name' => $item,
                'parent_id' => $extrusion_technomatic_125->id
            ]);
        }

        $extrusion_technomatic_63 = Equipment::create([
            'name' => 'Техноматик 63',
            'parent_id' => $extrusion->id,
        ]);

        foreach ($array_63 as $item){
            Equipment::create([
                'name' => $item,
                'parent_id' => $extrusion_technomatic_63->id
            ]);
        }

        $amut = Equipment::create([
            'name' => 'Амут',
            'parent_id' => $extrusion->id,
        ]);

        foreach ($array_amut as $item){
            Equipment::create([
                'name' => $item,
                'parent_id' => $amut->id
            ]);
        }

        $casting = Equipment::create([
            'name' => 'Литье',
            'parent_id' => null
        ]);

        Equipment::create([
            'name' => '220',
            'parent_id' => $casting->id
        ]);

        Equipment::create([
            'name' => '260',
            'parent_id' => $casting->id
        ]);

        Equipment::create([
            'name' => 'Пресс-формы',
            'parent_id' => $casting->id
        ]);

        $accessory = Equipment::create([
            'name' => 'Вспомогательное',
            'parent_id' => null
        ]);

        $accessory_array = [
            'Чиллер',
	        'Кессон',
	        'Насосная группа и трубопроводы',
	        'Компрессорная группа',
	        'Прочее',
        ];

        foreach ($accessory_array as $item){
            Equipment::create([
                'name' => $item,
                'parent_id' => $accessory->id
            ]);
        }
    }
}
