<?php

use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entities\Group::create([
            'name' => 'Без группы'
        ]);

        $admin = \App\Entities\Group::create([
            'name' => 'Администрация'
        ]);

        $actions = \App\Entities\Action::pluck('id');
        $admin->actions()->sync($actions->toArray());
    }
}
