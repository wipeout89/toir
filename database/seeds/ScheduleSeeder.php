<?php

use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entities\Schedule::create([
            'task_type' => \App\Entities\Schedule::SYNCHRONIZATION_TASK_AND_TASK_VIEW,
            'run_time' => '2019-04-09 07:25:00'
        ]);
    }
}
