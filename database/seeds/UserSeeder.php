<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    // Количество пользователей, сгенерируемых с помощью API randomuser.ru
    // Если 0 - генератор не запустится
    private $generator_users_count = 20;

    // Группа для сгенерированных пользователей по умолчанию
    private $generator_users_group_id = 1;

    // Системные параметры для генератора пользователей
    private $get_opts = [
        'http' => [
            'method' => "GET",
            'timeout' => 1200,
            'header' => "Accept-language: en\r\n" .
                "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6\r\n"
        ]
    ];

    public function run()
    {
        \App\Entities\User::create([
            'name' => 'Валентин',
            'surname' => 'Иванов',
            'middle_name' => 'Олегович',
            'phone' => '+7(900)654-95-67',
            'password' => Hash::make('secret'),
            'group_id' => 2
        ]);

        \App\Entities\User::create([
            'name' => 'Григорий',
            'surname' => 'Ушаков',
            'middle_name' => 'Вячеславович',
            'phone' => '+7(921)785-92-45',
            'password' => Hash::make('secret'),
            'group_id' => 2
        ]);

        \App\Entities\User::create([
            'name' => 'Евгений',
            'surname' => 'Горбунцов',
            'middle_name' => '',
            'phone' => '+7(911)101-27-47',
            'password' => Hash::make('secret'),
            'group_id' => 2
        ]);

        \App\Entities\User::create([
            'name' => 'Павел',
            'surname' => 'Лебедев',
            'middle_name' => '',
            'phone' => '+7(921)745-84-68',
            'password' => Hash::make('secret'),
            'group_id' => 2
        ]);

        /*
        $generator_users_count = $this->generator_users_count;
        if ($generator_users_count > 0) {
            $context = stream_context_create($this->get_opts);
            for ($i = 0; $i < $generator_users_count; ++$i) {
                $json = json_decode(file_get_contents('http://randomuser.ru/api.json', false, $context));
                if (!empty($json)) {
                    \App\Entities\User::create([
                        'name' => ($json[0]->user->name->first),
                        'surname' => ($json[0]->user->name->last),
                        'middle_name' => ($json[0]->user->name->middle),
                        'phone' => '+7(' . random_int(100, 999) . ')' . random_int(100, 999) . '-' . random_int(10, 99) . '-' . random_int(10, 99),
                        'password' => Hash::make('secret'),
                        'group_id' => $this->generator_users_group_id
                    ]);
                }
            }
        }
        */
    }
}
