@extends('layouts.app')

@section('title', 'Авторизация')

@section('additional-body-class', 'h-menu')

@section('content')
    <div id="main-content">
        <div class="container">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-12 col-sm-12 align-center">
                        <h1>Авторизация</h1>
                    </div>
                </div>
            </div>

            <div class="row clearfix justify-content-center">
                <div class="col-lg-8 col-md-12">
                    <div class="card">
                        <form method="post" action="/login">
                            <div class="body">
                                {{csrf_field()}}
                                <label for="phone">Телефон</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" id="phone" name="phone" autocomplete="off">
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <label for="password">Пароль</label>
                                <div class="input-group mb-3">
                                    <input type="password" class="form-control" id="password" name="password" autocomplete="off">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <button class="btn btn-primary" type="submit">Вход</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection