@extends ('layouts.app')

@section('title', 'Добавление группы')

@section('additional_styles')
{{--    <link rel="stylesheet" href="{{asset('general_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}">--}}
{{--    <link rel="stylesheet"--}}
{{--          href="{{asset('general_assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">--}}
{{--    <link rel="stylesheet"--}}
{{--          href="{{asset('general_assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}">--}}
    <link rel="stylesheet" href="{{asset('general_assets/vendor/multi-select/css/multi-select.css')}}">
{{--    <link rel="stylesheet" href="{{asset('general_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">--}}
{{--    <link rel="stylesheet" href="{{asset('general_assets/vendor/nouislider/nouislider.min.css')}}">--}}
@endsection

@section('content')
    @include('layouts._header')
    @include('layouts._left-sidebar')
    <div id="main-content">
        <div class="container-fluid">

            @include('layouts._flash-message')

            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>@if(isset($group))Редактирование группы@else Добавление группы@endif</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Группы</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@if(isset($group))Редактирование группы@else Добавление группы@endif</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <form action="@if(isset($group)){{route('group.update')}}@else{{route('group.save')}}@endif" method="post">
                                @csrf
                                <input type="hidden" name="group_id" @if(isset($group))value="{{$group->id}}"@endif>
                                <label for="group-name">Название группы</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" id="group-name" name="group_name"
                                           placeholder="Название группы"
                                           aria-label="Название группы" @if(!empty($group->name))value="{{$group->name}}"@endif>
                                </div>
                                <label for="optgroup">Участники группы</label>
                                <div class="input-group mb-3">
                                    <select id="ms-users" name="ms_users[]" multiple="multiple">
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}" @if($user->group_id == $group->id) selected @endif>{{$user->surname}} {{$user->name}} {{$user->middle_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="multiselect2">Права группы</label>
                                <div class="input-group mb-3">
                                    <select id="ms-actions" name="ms_actions[]" multiple="multiple">
                                        @foreach($actions as $action)
{{--                                            @dd($action)--}}
                                            <option @if(in_array($action->name, $actions_group)) selected @endif>{{$action->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button class="btn btn-primary" type="submit">Сохранить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('additional_scripts')
{{--    <script src="{{asset('general_assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script><!-- Bootstrap Colorpicker Js -->--}}
{{--    <script src="{{asset('general_assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script><!-- Input Mask Plugin Js -->--}}
{{--    <script src="{{asset('general_assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js')}}"></script>--}}
    <script src="{{asset('general_assets/vendor/multi-select/js/jquery.multi-select.js')}}"></script><!-- Multi Select Plugin Js -->
{{--    <script src="{{asset('general_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>--}}
{{--    <script src="{{asset('general_assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>--}}
{{--    <script src="{{asset('general_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script><!-- Bootstrap Tags Input Plugin Js -->--}}
{{--    <script src="{{asset('general_assets/vendor/nouislider/nouislider.js')}}"></script><!-- noUISlider Plugin Js -->--}}
{{--    <script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script>--}}
{{--    <script src="{{asset('assets/js/pages/forms/advanced-form-elements.js')}}"></script>--}}
    <script>
        $('#ms-users').multiSelect();
        $('#ms-actions').multiSelect()
    </script>
@endsection