@extends('layouts.app')

@section('title', 'Группы')

@section('content')
    @include('layouts._header')
    @include('layouts._left-sidebar')

    <div id="main-content">
        <div class="container-fluid">
            @include('layouts._flash-message')

            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Список групп</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Группы</a></li>
                                <li class="breadcrumb-item"><a href="#">Просмотр</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class="accordion" id="accordion-groups">
                                @foreach($groups as $group)
                                    <div>
                                        <div class="card-header"  style="background: white" id="heading{{$group->id}}">
                                            <h5 class="mb-0">
                                                <button class="btn btn-default collapsed" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse{{$group->id}}-info" aria-expanded="false"
                                                        aria-controls="collapse{{$group->id}}">Название
                                                    группы: {{$group->name}}<br>
                                                    Cтатус группы: {{\App\Entities\Group::$statuses[$group->status]}}
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapse{{$group->id}}-info" style="background: white" class="collapse"
                                             aria-labelledby="heading{{$group->id}}"
                                             data-parent="#accordion-groups">
                                            <div class="body">
                                                <div class="row">
                                                    <div class="col-12 col-sm-3">
                                                        <div class="nav flex-column nav-pills" id="v-pills-tab"
                                                             role="tablist" aria-orientation="vertical">
                                                            <a class="nav-link active show" id="v-pills-users-tab-group-{{$group->id}}"
                                                               data-toggle="pill" href="#v-pills-users-group-{{$group->id}}" role="tab"
                                                               aria-controls="v-pills-users-group-{{$group->id}}" aria-selected="true">Пользователи</a>
                                                            <a class="nav-link" id="v-pills-rights-tab-group-{{$group->id}}"
                                                               data-toggle="pill" href="#v-pills-rights-group-{{$group->id}}" role="tab"
                                                               aria-controls="v-pills-rights-group-{{$group->id}}" aria-selected="false">Права</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-9">
                                                        <div class="tab-content" id="v-pills-tabContent">
                                                            <div class="tab-pane active show" id="v-pills-users-group-{{$group->id}}"
                                                                 role="tabpanel" aria-labelledby="v-pills-users-tab-group-{{$group->id}}">
                                                                <label for="list-group-users-group-{{$group->id}}">Участники группы:</label>
                                                                @foreach($users as $user)
                                                                    @if($user->group_id == $group->id)
                                                                        <ul class="list-group" id="list-group-users-group-{{$group->id}}">
                                                                            <li class="list-group-item">
                                                                                {{$user->surname}} {{$user->name}} {{$user->middle_name}}
                                                                            </li>
                                                                        </ul>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                            <div class="tab-pane" id="v-pills-rights-group-{{$group->id}}" role="tabpanel"
                                                                 aria-labelledby="v-pills-rights-tab-group-{{$group->id}}">
                                                                <label for="list-group-rights-group-{{$group->id}}">Права группы:</label>
                                                                @foreach($actions_groups as $action_group)
{{--                                                                    @dd($actions_groups)--}}
                                                                    @if($action_group->group_id == $group->id)
                                                                        <ul class="list-group" id="list-group-rights-group-{{$group->id}}">
                                                                            <li class="list-group-item">
                                                                                {{$action_group->action_name}}
                                                                            </li>
                                                                        </ul>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <form method="get" action="{{$group->id}}/edit">
                                                    <button class="btn btn-primary mt-3" id="button-edit-group-{{$group->id}}" type="submit">Редактировать группу</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('additional_scripts')
@endsection