@extends ('layouts.app')

@section('title', 'Добавление задачи')

@section('additional_styles')
    <link rel="stylesheet" href="{{asset('general_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}">
    <link rel="stylesheet"
          href="{{asset('general_assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('general_assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}">
    <link rel="stylesheet" href="{{asset('general_assets/vendor/multi-select/css/multi-select.css')}}">
    <link rel="stylesheet" href="{{asset('general_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{asset('general_assets/vendor/nouislider/nouislider.min.css')}}">
@endsection

@section('content')
    @include('layouts._header')
    @include('layouts._left-sidebar')
    <div id="main-content">
        <div class="container-fluid">

            @include('layouts._flash-message')

            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Добавление задач</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Задачи</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Добавление задач</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <form action="{{(Route::currentRouteName() != 'inspection.create-ticket') ? route('inspection.save') : route('inspection.save-ticket')}}" method="post">
                                @csrf
                                @if(Route::currentRouteName() != 'inspection.create-ticket')
                                    <label for="title">Проводимые работы</label>
                                    <div class="input-group mb-3">
                                        <textarea class="form-control" id="title" name="title"
                                                  placeholder="Проводимые работы"></textarea>
                                    </div>
                                @else
                                    <label for="title">Описание проблемы</label>
                                    <div class="input-group mb-3">
                                        <textarea class="form-control" id="title" name="title"
                                                  placeholder="Введите описание проблемы"></textarea>
                                    </div>
                                @endif

                                <label>Оборудование</label>
                                <p id="equipment-hint"></p>
                                <select id="equipment" class="form-control mb-3">
                                    <option value="-1" selected disabled>Не выбрано</option>
                                    @foreach($equipments as $equipment)
                                        <option value="{{$equipment->id}}">{{$equipment->name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="equipment_id" name="equipment_id">

                                @if(Route::currentRouteName() != 'inspection.create-ticket')
                                    <label for="period">Период</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <button id="units-button" class="btn btn-outline-secondary dropdown-toggle" type="button"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Единицы измерения
                                            </button>
                                            <div class="dropdown-menu">
                                                @foreach(\App\Entities\InspectionTask::$units as $key=>$unit)
                                                    <a class="dropdown-item" id="{{$key}}" href="javascript:void(0);">{{$unit}}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                        <input type="number" class="form-control" id="period" name="period"
                                               placeholder="Период">
                                    </div>
                                    <input type="hidden" id="units" name="units">
                                @endif

                                <button class="btn btn-primary" type="submit">Сохранить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script src="{{asset('general_assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script><!-- Bootstrap Colorpicker Js -->
    <script src="{{asset('general_assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script><!-- Input Mask Plugin Js -->
    <script src="{{asset('general_assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js')}}"></script>
    <script src="{{asset('general_assets/vendor/multi-select/js/jquery.multi-select.js')}}"></script><!-- Multi Select Plugin Js -->
    <script src="{{asset('general_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
    <script src="{{asset('general_assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('general_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script><!-- Bootstrap Tags Input Plugin Js -->
    <script src="{{asset('general_assets/vendor/nouislider/nouislider.js')}}"></script><!-- noUISlider Plugin Js -->
    <script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{asset('assets/js/pages/forms/advanced-form-elements.js')}}"></script>

    <script>
        $('#equipment').on('change', function () {
            let equipment_id = $('#equipment').val();
            $('#equipment_id').val(equipment_id);
            $.ajax({
                url: "{{route('task.get-equipment')}}",
                method: 'get',
                data: {id: $('#equipment').val()},
                success: function (json) {
                    let json1 = JSON.parse(json);

                    console.log(json1);
                    let equipment = json1.equipment;
                    $('#equipment-hint').html(json1.full_name);
                    if(equipment.length !== 0) {
                        let html = '<option value="-1" selected disabled>Не выбрано</option>';
                        equipment.forEach(function (item) {
                            html += '<option value="' + item.id + '">' + item.name + '</option>';
                        });
                        $('#equipment').html(html);
                    }
                }
            })
        });
        $('.dropdown-item').on('click', function () {
            $('#units').val($(this).attr('id'));
            $('#units-button').text($(this).text());
        })
    </script>
@endsection