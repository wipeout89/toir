@extends('layouts.app')

@section('title', 'Задачи')

@section('content')
    @include('layouts._header')
    @include('layouts._left-sidebar')

    <div id="main-content">
        <div class="container-fluid">

            @include('layouts._flash-message')

            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h2>Обход</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active" aria-current="page">Обход</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="createTicketModal" tabindex="-1" role="dialog" aria-labelledby="createTicketModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Создание заявки на обслуживание</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{route('inspection.create-ticket')}}" id="create-ticket-form">
                                @csrf
                                <input type="hidden" name="task_id" id="form-task-id">
                                <label for="commentary">Введите комментарий</label>
                                <div class="input-group mb-3">
                                    <textarea class="form-control" name="commentary" id="commentary" rows="3" placeholder="Введите комментарий"></textarea>
                                </div>
                                <label for="commentary">Укажите требуемые задачи</label>
                                <div class="input-group mb-3">
                                    <textarea class="form-control" name="spares" id="spares" rows="3" placeholder="Требуемые запчасти"></textarea>
                                </div>
                                <div class="fancy-checkbox mb-3">
                                    <label><input name="with_stop" type="checkbox"><span>Требуется ли остановка оборудования?</span></label>
                                </div>
                                <label for="commentary">Требуемая квалификация</label>
                                <select class="form-control" name="group_id">
                                    @foreach(\App\Entities\Group::where('id', '<>', 1)->get() as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Закрыть</button>
                            <form method="post" action="{{route('task.close-task')}}">
                                <button type="button" id="create-ticket-button" class="btn btn-round btn-primary">Создать</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-hover table-custom spacing5">
                                <thead>
                                <tr>
                                    <th style="width: 10px"></th>
                                    <th>Описание <wbr>работ</th>
                                    <th class="w45 d-none d-sm-table-cell">Оборудование</th>
                                    <th class="w100 text-right d-none d-md-table-cell"></th>
                                    <th class="w100"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tasks as $task)
                                    <tr id="row-{{$task->id}}">
                                        <td>
                                            <label class="fancy-checkbox" style="margin: 0">
                                                <input id="checkbox-{{$task->id}}" type="checkbox" name="checkbox" disabled>
                                                <span></span>
                                            </label>
                                        </td>
                                        <td class="d-sm-none">{!! str_replace(' ', " <wbr>", $task->title) !!} <br>Оборудование:<br>{!! str_replace('/', "/<wbr>", $task->equipment->full_name) !!}</td>
                                        <td class="d-none d-sm-table-cell">{{$task->title}}</td>
                                        <td class="d-none d-sm-table-cell">
                                            {!! str_replace('/', "/<wbr>", $task->equipment->full_name) !!}
                                        </td>
                                        <td class="text-right d-none d-md-table-cell">
                                            <button type="submit" id="btn-success-1-{{$task->id}}" class="btn btn-round btn-success p-1 pl-2 pr-2" data-task-id="{{$task->id}}">Проверено</button>
                                        </td>
                                        <td>
                                            <div class="input-group mb-2 d-md-none ">
                                                <button type="submit" id="btn-success-2-{{$task->id}}" class="btn btn-round btn-success p-1 pl-2 pr-2" data-task-id="{{$task->id}}">Проверено</button>
                                            </div>
                                            <div class="input-group mb-2 mb-md-0">
                                                <button id="btn-alert-{{$task->id}}" class="btn btn-round btn-danger p-1 pl-2 pr-2" data-toggle="modal" data-task-id="{{$task->id}}" data-target="#createTicketModal">Создать тикет</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script>
        let checkboxes = [];
        $('.btn-success').on('click', function (e) {
            let button = e.target;
            let id = $(button).data('task-id');
            let checkbox = $("#checkbox-" + id);
            if(checkbox.prop('checked')){
                checkbox.prop('checked', false);
                $('#btn-alert-' + id).show();
                changeStatusInspectionTask(id);
                $(button).text('Проверено');
            } else {
                checkbox.prop('checked', true);
                $('#btn-alert-' + id).hide();
                changeStatusInspectionTask(id);
                $(button).text('Отменить');
            }
        });

        $('#send-form').on('click', function () {
            $('#ids').val(checkboxes);
            $('#close-inspection').submit();
        });

        $('.modal').on('show.bs.modal', function (e) {
            let button = e.relatedTarget;
            $('#form-task-id').val($(button).data('task-id'));
        });

        $('#createTicketModal').on('hide.bs.modal', function (e) {
            $('#create-ticket-form')[0].reset();
        });



        $("#create-ticket-button").on('click', function(){
            $.ajax({
                url: '{{route('inspection.create-ticket')}}',
                type: 'post',
                data: $('#create-ticket-form').serialize(),
                success: function(json) {
                    let decode_json = JSON.parse(json);
                    if(decode_json.result){
                        let checkbox = $("#checkbox-" + decode_json.id);
                        checkbox.prop('checked', true);
                        $('#success-flash-message').show();
                        $('#success-flash-message-text').text('Тикет успешно создан');
                        $('#createTicketModal').modal('toggle');
                        $('#btn-success-1-'+decode_json.id+', #btn-success-2-'+decode_json.id+', #btn-alert-'+decode_json.id).remove();
                        changeStatusInspectionTask(decode_json.id);
                    } else {
                        $('#createTicketModal').modal('toggle');
                        $('#alert-flash-message').show();
                        $('#alert-flash-message-text').text('Тикет не создан попробуйте ещё раз')
                    }
                },
                error: function() {
                    $('#createTicketModal').modal('toggle');
                    $('#alert-flash-message').show();
                    $('#alert-flash-message-text').text('Тикет не создан попробуйте ещё раз')
                }
            });
        });

        function changeStatusInspectionTask(id) {
            let result;
            $.ajax({
                url: '{{route('inspection.swap-status')}}',
                type: 'post',
                data: {_token:'{{ csrf_token() }}', id:id},
            });
        }
    </script>
@endsection