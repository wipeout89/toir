<div class="row clearfix">
    <div class="col-lg-12">
{{--        @if(session('type') == 'success')--}}
            <div class="alert alert-success alert-dismissible" role="alert" id="success-flash-message"
                 style="display: @if(session('type') == 'success') block @else none @endif">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check-circle"></i> <span id="success-flash-message-text">{{session('message')}}</span>
            </div>
{{--        @endif--}}
{{--        @if(session('type'))--}}
            <div class="alert alert-danger alert-dismissible" role="alert" id="alert-flash-message"
                 style="display: @if(session('type') == 'alert') block @else none @endif">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-times-circle"></i> <span id="alert-flash-message-text">{{session('message')}}</span>
            </div>
{{--        @endif--}}
    </div>
</div>