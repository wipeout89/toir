<nav class="navbar top-navbar">
    <div class="container-fluid">

        <div class="navbar-left">
            <div class="navbar-btn">
                <a href="{{url('/')}}"><img src="{{asset('general_assets/images/icon.svg')}}" alt="Oculux Logo" class="img-fluid logo"></a>
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>
        </div>

{{--        <div class="navbar-right">--}}
{{--            <div id="navbar-menu">--}}
{{--                <ul class="nav navbar-nav">--}}
{{--                    <li><a style="color: black" href="{{ route('logout') }}"--}}
{{--                           onclick="event.preventDefault();--}}
{{--                                    document.getElementById('logout-form').submit();"><i class="icon-power"></i></a></li>--}}
{{--                </ul>--}}
{{--                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                    @csrf--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    <div class="progress-container"><div class="progress-bar" id="myBar"></div></div>
</nav>