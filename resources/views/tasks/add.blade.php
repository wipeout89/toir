@extends ('layouts.app')

@section('title', 'Добавление задачи')

@section('additional_styles')
    <link rel="stylesheet" href="{{asset('general_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}">
    <link rel="stylesheet"
          href="{{asset('general_assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('general_assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}">
    <link rel="stylesheet" href="{{asset('general_assets/vendor/multi-select/css/multi-select.css')}}">
    <link rel="stylesheet" href="{{asset('general_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{asset('general_assets/vendor/nouislider/nouislider.min.css')}}">
@endsection

@section('content')
    @include('layouts._header')
    @include('layouts._left-sidebar')
    <div id="main-content">
        <div class="container-fluid">

            @include('layouts._flash-message')

            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Добавление задач</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('task.show-all')}}">Задачи</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Создание тикета</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <form action="{{(Route::currentRouteName() != 'task.create-ticket') ? route('task.save') : route('task.save-ticket')}}" method="post">
                                @csrf
                                @if(Route::currentRouteName() != 'task.create-ticket')
                                    <label for="title">Название проводимых работ</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Название проводимых работ">
                                    </div>
                                @endif
                                @if(Route::currentRouteName() != 'task.create-ticket')
                                    <label for="description">Описание проводимых работ</label>
                                    <div class="input-group mb-3">
                                        <textarea class="form-control" name="description" id="description" rows="3"
                                              placeholder="Введите описание проводимых работ"></textarea>
                                    </div>
                                @else
                                    <label for="description">Описание проблемы</label>
                                    <div class="input-group mb-3">
                                        <textarea class="form-control" name="description" id="description" rows="3"
                                              placeholder="Описание проблемы"></textarea>
                                    </div>
                                @endif

                                <label>Оборудование</label>
                                <p id="equipment-hint"></p>
                                <select id="equipment" class="form-control mb-3">
                                    <option value="-1" selected disabled>Не выбрано</option>
                                    @foreach($equipments as $equipment)
                                        <option value="{{$equipment->id}}">{{$equipment->name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="equipment_id" name="equipment_id">

                                <label for="spares">Требуемые запчасти</label>
                                <div class="input-group mb-3">
                                    <textarea class="form-control" id="spares" name="spares"
                                              placeholder="Описание"></textarea>
                                </div>

                                @if(Route::currentRouteName() != 'task.create-ticket')
                                    <label for="period">Период</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <button id="units-button" class="btn btn-outline-secondary dropdown-toggle" type="button"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Единицы измерения
                                            </button>
                                            <div class="dropdown-menu">
                                                @foreach(\App\Entities\Task::$units as $key=>$unit)
                                                    <a class="dropdown-item" id="{{$key}}" href="javascript:void(0);">{{$unit}}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                        <input type="number" class="form-control" id="period" name="period"
                                               placeholder="Период">
                                    </div>
                                    <input type="hidden" id="units" name="units">
                                @endif

                                <label>Квалификация</label>
                                <select id="qualification" name="group_id"
                                        class="form-control mb-3">
                                    @foreach(\App\Entities\Group::where('id', '<>', 1)->get() as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>

                                @if(Route::currentRouteName() != 'task.create-ticket')
                                    <div>
                                        <label>Дата первого выполнения</label>
                                        <div class="input-group mb-3">
                                            <input data-provide="datepicker" name="run_time" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" autocomplete="false">
                                        </div>
                                    </div>
                                @endif

                                <div class="fancy-checkbox mb-3">
                                    <label><input name="with_stop" type="checkbox"><span>Требуется ли остановка оборудования?</span></label>
                                </div>

                                <button class="btn btn-primary" type="submit">Сохранить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('additional_scripts')
{{--    <script src="{{asset('general_assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script><!-- Bootstrap Colorpicker Js -->--}}
{{--    <script src="{{asset('general_assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script><!-- Input Mask Plugin Js -->--}}
{{--    <script src="{{asset('general_assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js')}}"></script>--}}
{{--    <script src="{{asset('general_assets/vendor/multi-select/js/jquery.multi-select.js')}}"></script><!-- Multi Select Plugin Js -->--}}
{{--    <script src="{{asset('general_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>--}}
    <script src="{{asset('general_assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
{{--    <script src="{{asset('general_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script><!-- Bootstrap Tags Input Plugin Js -->--}}
{{--    <script src="{{asset('general_assets/vendor/nouislider/nouislider.js')}}"></script><!-- noUISlider Plugin Js -->--}}
{{--    <script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script>--}}
{{--    <script src="{{asset('assets/js/pages/forms/advanced-form-elements.js')}}"></script>--}}

    <script>
        $('#equipment').on('change', function () {
            let equipment_id = $('#equipment').val();
            $('#equipment_id').val(equipment_id);
            $.ajax({
                url: "{{route('task.get-equipment')}}",
                method: 'get',
                data: {id: equipment_id},
                success: function (json) {
                    let json1 = JSON.parse(json);
                    let equipment = json1.equipment;
                    $('#equipment-hint').html(json1.full_name);
                    if(equipment.length !== 0) {
                        let html = '<option value="-1" selected disabled>Не выбрано</option>';
                        equipment.forEach(function (item) {
                            html += '<option value="' + item.id + '">' + item.name + '</option>';
                        });
                        $('#equipment').html(html);
                    }
                }
            })
        });
        $('.dropdown-item').on('click', function () {
            $('#units').val($(this).attr('id'));
            $('#units-button').text($(this).text());
        })
    </script>
@endsection