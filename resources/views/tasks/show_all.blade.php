@extends('layouts.app')

@section('title', 'Задачи')

@section('content')
    @include('layouts._header')
    @include('layouts._left-sidebar')

    <div id="main-content">
        <div class="container-fluid">
            @include('layouts._flash-message')
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Необходимые работы</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Задачи</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Просмотр</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="moreModal" tabindex="-1" role="dialog" aria-labelledby="moreModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modal-title">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p class="font-weight-bold">Необходимые запчасти:</p>
                            <p class="ml-3" id="spares"></p>
                            <p class="font-weight-bold">Оборудование:</p>
                            <p class="ml-3" id="equipment"></p>
                            <p class="font-weight-bold">Требуемая квалификация:</p>
                            <p class="ml-3" id="qualification"></p>
                            <p class="font-weight-bold">Требуется остановка оборудования:</p>
                            <p class="ml-3" id="with_stop"></p>
                            <p class="font-weight-bold">Дата планового выполнения:</p>
                            <p class="ml-3" id="run_time"></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Закрыть</button>
                            <form method="post" action="{{route('task.close-task')}}">
                                <input type="hidden" name="task_id" id="form-task-id">
                                @csrf
                                <button type="submit" class="btn btn-round btn-primary">Подтвердить выполнение</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-12">
                    <div class="card">
                        <div class="body">
                            <form action="{{route('task.show-all')}}" method="get">
                                <div class="row">
                                    <div class="col-12 col-sm-5 col-md-4 col-lg-3 col-xl-2 align-self-end">
                                        <label>Количество дней вывода:</label>
                                    </div>
                                    <input class="col-4 form-control ml-3 col-sm-2 ml-sm-0 col-md-1" type="number" name="days" value="{{$days_before_task}}">
                                    <button type="submit" class="btn btn-default col-6 ml-2 col-sm-3 ml-sm-3 col-md-2">Применить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <table id="only-bodytable" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Название <wbr>работ</th>
                                    <th class="d-none d-md-table-cell">Оборудование</th>
                                    <th class="d-none d-lg-table-cell">Квалификация</th>
                                    <th class="d-none d-md-table-cell">Плановая <wbr>дата</th>
                                    <th class="d-none d-md-table-cell">Остановка</th>
                                    <th></th>
                                </tr>
                                </thead>
                                @foreach($tasks as $task)
                                    <tr>
                                        <td>{!! str_replace(' ', " <wbr>", $task->title) !!}<i class="table-dragger-handle sindu_handle"></i></td>
                                        <td class="d-none d-md-table-cell">{!! str_replace('/', "/<wbr>", $task->equipment->full_name) !!}</td>
                                        <td class="d-none d-lg-table-cell">{{$task->qualification->name}}</td>
                                        <td class="d-none d-md-table-cell">{{$task->run_time}}</td>
                                        <td class="d-none d-md-table-cell">
                                            <div class="fancy-checkbox">
                                                <label><input type="checkbox" @if($task->with_stop) checked @endif disabled readonly><span></span></label>
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-round btn-primary p-1 p-sm-2 pl-sm-3 pr-sm-3" data-task-id="{{$task->id}}" data-toggle="modal" data-header="{{$task->title}}" data-target="#moreModal">Подробнее</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script src="{{asset('general_assets/vendor/table-dragger/table-dragger.min.js')}}"></script>
    <script>
        let dragger = tableDragger(document.querySelector("#only-bodytable"), { mode: "row", onlyBody: true });
        dragger.on('drop', function (oldIndex, newIndex) {
            console.log(oldIndex);
            console.log(newIndex);
            $.ajax({
                url: '/task/change-rank',
                method: 'get',
                data: {newIndex:newIndex, oldIndex:oldIndex},
            });
        });

        $('.modal').on('show.bs.modal', function (e) {
            let button = e.relatedTarget;
            $('#modal-title').text($(button).data('header'));
            $('#form-task-id').val($(button).data('task-id'));

            $.ajax({
                url: '{{route('task.get-task')}}',
                method: 'get',
                data: {id: $(button).data('task-id')},
                success: function (json) {
                    let task = JSON.parse(json);
                    $('#spares').text(task.spares);
                    $('#equipment').text(task.equipment.full_name);
                    $('#qualification').text(task.qualification.name);
                    $('#with_stop').text((task.with_stop) ? "Да" : "Нет");
                    $('#run_time').text(task.run_time);
                }
            })
        });
    </script>
@endsection