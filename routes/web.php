<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::prefix('group')->group(function (){
        Route::get('add', 'GroupController@add');
        Route::get('/{id}/edit', 'GroupController@edit');
        Route::post('save', 'GroupController@save')->name('group.save');
        Route::post('update', 'GroupController@update')->name('group.update');
        Route::get('show-all', 'GroupController@showAll')->name('group.show-all');
    });

    Route::prefix('inspection')->group(function (){
        Route::get('add', 'InspectionTaskController@add');
        Route::post('save', 'InspectionTaskController@save')->name('inspection.save');
        Route::get('/', 'InspectionTaskController@checkList')->name('inspection.index');
        Route::get('get-equipment', 'InspectionTaskController@getEquipment')->name('inspection.get-equipment');
        Route::post('close-inspection', 'InspectionTaskController@close')->name('inspection.close-inspection');
        Route::post('create-ticket', 'InspectionTaskController@createTicket')->name('inspection.create-ticket');
        Route::post('swap-status', 'InspectionTaskController@swapStatus')->name('inspection.swap-status');
        Route::get('create-ticket', 'InspectionTaskController@add')->name('inspection.create-ticket');
        Route::post('create-ticket', 'InspectionTaskController@saveTicket')->name('inspection.save-ticket');
    });

    Route::prefix('task')->group(function (){
        Route::get('add', 'TaskController@add');
        Route::post('save', 'TaskController@save')->name('task.save');
        Route::get('show-all', 'TaskController@showAll')->name('task.show-all');
        Route::get('get-equipment', 'TaskController@getEquipment')->name('task.get-equipment');
        Route::get('change-rank', 'TaskController@changeRank')->name('task.change-alias');
        Route::get('get-task', 'TaskController@getTask')->name('task.get-task');
        Route::post('close-task', 'TaskController@closeTask')->name('task.close-task');
        Route::get('create-ticket', 'TaskController@add')->name('task.create-ticket');
        Route::post('create-ticket', 'TaskController@saveTicket')->name('task.save-ticket');
    });


});

